#! /usr/bin/env python

import shelve
import csv
import pickle


greek = {"Alp": "\u03b1", "Bet": "\u03b2", "Gam": "\u03b3", "Del": "\u03b4", "Eps": "\u03b5",
         "Zet": "\u03b6", "Eta": "\u03b7", "The": "\u03b8", "Iot": "\u03b9", "Kap": "\u03ba",
         "Lam": "\u03bb", "Mu": "\u03bc", "Nu": "\u03bd", "Xi": "\u03be", "Omi": "\u03bf",
         "Pi": "\u03c0", "Rho": "\u03c1", "Sig": "\u03c3", "Tau": "\u03c4", "Ups": "\u03c5",
         "Phi": "\u03c6", "Chi": "\u03c7", "Psi": "\u03c8", "Ome": "\u03c9"}
def fix_name(name):
    orig_name = name[:]
    for i in range(10):
        name = name.replace(str(i), " ")
    name = name.strip()
    if len(name.split()) == 1:
        return " ".join(orig_name.split())
    if name.split()[0] in greek:
        new_name = greek[name.split()[0]] + " " + name.split()[1]
        return " ".join(new_name.split())


cc = []
for line in open("constellation_lines.dat"):
    if line.startswith("M") or line.startswith("D"):
        cc.append(int(line.split()[1]))

stars_dict = {"bright": {}}
with open("hygdata_v3.csv") as data_file:
    csv_reader = csv.reader(data_file)
    for idx, row in enumerate(csv_reader):
        if idx == 0:
            continue
        else:
            if row[2] != "":
                hd = int(row[2])
            else:
                continue
            if row[7] != "":
                ra = 15*float(row[7])
            else:
                continue
            if row[8] != "":
                dec = float(row[8])
            else:
                continue
            if row[13] != "":
                mag = float(row[13])
            else:
                mag = 99
            if row[6] != "":
                name = row[6]
            elif row[5] != "":
                name = row[5]
            else:
                name = f"HD {hd}"
            name = fix_name(name)
            if (hd in cc):
                stars_dict["bright"][hd] = (ra, dec, mag)

            if (mag != 0) and (mag <= 10):
                group = "m_%i" % int(mag)
            else:
                continue
            if group not in stars_dict:
                stars_dict[group] = {"ra": [], "dec": [], "mag": [], "name": []}
            stars_dict[group]["ra"].append(ra)
            stars_dict[group]["dec"].append(dec)
            stars_dict[group]["mag"].append(mag)
            stars_dict[group]["name"].append(name)

db = {}  # shelve.open("hd_catalogue.db")
db["stars"] = stars_dict

constellations = {}
constellations["all_ra"] = []
constellations["all_dec"] = []
idx = 0
for line in open("constellation_lines.dat"):
    if line.startswith("M"):
        if len(constellations["all_ra"]) != 0:
            constellations["all_ra"].append(0.0)
            constellations["all_dec"].append(-90)
        node = int(line.split()[1])
    elif line.startswith("D"):
        node = int(line.split()[1])
    else:
        continue
    print(node)
    node_ra, node_dec, _ = stars_dict["bright"][node]
    constellations["all_ra"].append(node_ra)
    constellations["all_dec"].append(node_dec)
print(constellations)
db["lines"] = constellations
# db.close()
pickle.dump(db, open("test.pkl", "wb"))
