#! /usr/bin/env python

from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal


class TimeControls(QtWidgets.QWidget):
    time_shift_button_pressed = pyqtSignal(int)

    def __init__(self, parent):
        super().__init__(parent)
        self.layout = QtWidgets.QHBoxLayout()

        self.backward_button = QtWidgets.QPushButton("\u2190")
        self.forward_button = QtWidgets.QPushButton("\u2192")
        self.set_current_button = QtWidgets.QPushButton("\u2193")

        self.layout.addWidget(self.backward_button)
        self.layout.addWidget(self.set_current_button)
        self.layout.addWidget(self.forward_button)

        self.setLayout(self.layout)

        # Emit signals on buttons pressing
        self.backward_button.clicked.connect(lambda: self.time_shift_button_pressed.emit(-1))
        self.forward_button.clicked.connect(lambda: self.time_shift_button_pressed.emit(1))
        self.set_current_button.clicked.connect(lambda: self.time_shift_button_pressed.emit(0))
