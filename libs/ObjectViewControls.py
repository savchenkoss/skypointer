import math
import json
from pathlib import Path

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt


class VisibilityWindow(QtWidgets.QWidget):
    def __init__(self, main_window):
        super().__init__()
        self.setWindowTitle("Select objects to show")
        icon_path = str(Path(__file__).parent / "icon.svg")
        self.setWindowIcon(QtGui.QIcon(icon_path))

        self.main_window = main_window
        # Make an array of checkboxes
        self.object_names = main_window.sky_plot.obs_objects.objects_names
        n_objects = len(self.object_names)
        n_columns = 10
        n_rows = math.ceil(n_objects / n_columns)

        main_layout = QtWidgets.QVBoxLayout()
        checkbox_layout = QtWidgets.QHBoxLayout()
        self.checkboxes = []
        for column in range(n_columns):
            column_layout = QtWidgets.QVBoxLayout()
            for row in range(n_rows):
                obj_idx = column*n_rows+row
                if obj_idx >= n_objects:
                    column_layout.addStretch()
                    break
                # panel_label = QtWidgets.QLabel(str(obj_idx))
                objname = self.object_names[obj_idx]
                checkbox = QtWidgets.QCheckBox(objname)
                if main_window.sky_plot.obs_objects.objects_visibility[objname] == 1:
                    checkbox.setChecked(True)
                column_layout.addWidget(checkbox)
                self.checkboxes.append(checkbox)
            checkbox_layout.addLayout(column_layout, Qt.AlignLeft)
        main_layout.addLayout(checkbox_layout)
        buttons_layout = QtWidgets.QHBoxLayout()
        ok_button = QtWidgets.QPushButton("Ok")
        ok_button.clicked.connect(self.apply_changes)
        buttons_layout.addWidget(ok_button, Qt.AlignLeft)
        buttons_layout.addStretch()
        cancel_button = QtWidgets.QPushButton("Cancel")
        cancel_button.clicked.connect(self.close)
        buttons_layout.addWidget(cancel_button, Qt.AlignLeft)
        main_layout.addLayout(buttons_layout)
        self.setLayout(main_layout)

    def apply_changes(self):
        for objname, checkbox in zip(self.object_names, self.checkboxes):
            if checkbox.isChecked():
                self.main_window.sky_plot.obs_objects.objects_visibility[objname] = 1
            else:
                self.main_window.sky_plot.obs_objects.objects_visibility[objname] = 0
        self.main_window.sky_plot.obs_objects.save()
        self.main_window.sky_plot.plot()
        self.close()
