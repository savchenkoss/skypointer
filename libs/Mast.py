#! /usr/bin/env python

import numpy as np
from astropy.coordinates import SkyCoord
from astropy.coordinates import AltAz
from .SkyObject import SkyObject


class Mast(SkyObject):
    """
    A class for plotting objects
    """
    def __init__(self, location):
        super().__init__(location)
        alt = np.linspace(39, 0, 15)
        az = np.ones_like(alt) * 241.5
        self.horizonthal_coords = SkyCoord(alt=alt, az=az, frame=AltAz(), unit="deg")
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        self.x_on_plot = zenith_distance * np.sin(azimuth)
        self.y_on_plot = - zenith_distance * np.cos(azimuth)

    def plot(self, ax):
        self.remove()
        plt_inst = ax.plot(self.x_on_plot, self.y_on_plot, "0.45", linewidth=2)[0]
        self.plot_instances.append(plt_inst)
        plt_inst = ax.plot(self.x_on_plot[:2], self.y_on_plot[:2], "0.45", linewidth=8)[0]
        self.plot_instances.append(plt_inst)

    def update_equatorial(self, obstime):
        # AltAz coordinates do not depend on time
        pass

    def compute_altaz(self, obstime):
        # AltAz coordinates do not depend on time
        pass
