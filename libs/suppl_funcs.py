#! /usr/bin/env python

from datetime import datetime


def get_decimal_year():
    now = datetime.now().timetuple()
    return now.tm_year + now.tm_yday / 365.25
