#! /usr/bin/env python

import time
import numpy as np
from astropy.coordinates import get_body
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from astropy.coordinates import AltAz
from astropy.time import Time
from .SkyObject import SkyObject


class Moon(SkyObject):
    """
    A class for plotting a mark on the sky that represent the current
    telescope location or user specified point.
    """
    def __init__(self, location):
        super().__init__(location)
        self.equatorial_coords = SkyCoord(ra=0.0, dec=-90, frame=FK5(equinox="J2000"), unit="deg")
        self.last_update = 0

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        if zenith_distance > np.pi / 2:
            # Marker is below the horizon
            return
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        x = zenith_distance * np.sin(azimuth)
        y = - zenith_distance * np.cos(azimuth)
        x_lim_min, x_lim_max = ax.get_xlim()
        delta_x = x_lim_max - x_lim_min
        size = max(10, 10 / delta_x)
        plt_inst = ax.plot(x, y, marker="o", markersize=int(size), markeredgecolor="0.75",
                           markerfacecolor="0.75")[0]
        self.plot_instances.append(plt_inst)
        plt_inst = ax.annotate("Moon", xy=(x, y), size=8)
        self.plot_instances.append(plt_inst)

    def moon_phase_angle(self, obstime):
        """
        Calculate lunar orbital phase in radians.
        Credits to https://astroplan.readthedocs.io/en/latest/_modules/astroplan/moon.html
        Parameters
        ----------
        obstime : `~astropy.time.Time`
        Time of observation

        Returns
        -------
        i : float
        Phase angle of the moon [radians]
        """
        obstime = Time("2019-01-27T06:10:00.00001", format="isot")
        sun = get_body(body="sun", time=obstime)
        moon = get_body(body="moon", time=obstime)
        elongation = sun.separation(moon)
        print(elongation.deg)
        return np.arctan2(sun.distance*np.sin(elongation), moon.distance - sun.distance*np.cos(elongation))

    def update_equatorial(self, obstime):
        self.equatorial_coords = get_body(body="moon", time=obstime, location=self.location)
        self.last_update = time.time()

    def compute_altaz(self, obstime):
        if time.time() - self.last_update > 600:
            # We only want to update the planets coordinates once 10 minutes
            self.update_equatorial(obstime)
        frame = AltAz(obstime=obstime, location=self.location)
        self.horizonthal_coords = self.equatorial_coords.transform_to(frame)
