#! /usr/bin/env python

import json
from collections import defaultdict
from pathlib import Path
import numpy as np
from scipy.ndimage import minimum_position
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from .SkyObject import SkyObject
from .suppl_funcs import get_decimal_year


class ObsObjects(SkyObject):
    """
    A class for plotting objects
    """
    def __init__(self, location):
        super().__init__(location)
        # Load visibility file
        self.visibility_file = Path(__file__).parent.parent / "data/visibility.json"
        self.objects_visibility = defaultdict(lambda: 1)
        if self.visibility_file.exists():
            with open(self.visibility_file) as fin:
                try:
                    self.objects_visibility.update(json.load(fin))
                except json.decoder.JSONDecodeError:
                    pass

        # Load TABOB file
        tabob_file = Path(__file__).parent.parent / "data" / "TABOB.TXT"
        self.objects_names = []
        self.inds_on_plot = []
        objects_ra = []
        objects_dec = []
        for line in open(tabob_file):
            object_name = line.split()[0]
            ra_h = float(line.split()[1])
            ra_m = float(line.split()[2])
            ra_s = float(line.split()[3])
            dec_d = float(line.split()[4])
            dec_m = float(line.split()[5])
            dec_s = float(line.split()[6])
            ra_decimal_deg = 15 * (ra_h + ra_m / 60 + ra_s / 3600)
            if dec_d > 0:
                dec_decimal_deg = dec_d + dec_m / 60 + dec_s / 3600
            else:
                dec_decimal_deg = dec_d - dec_m / 60 - dec_s / 3600
            self.objects_names.append(object_name)
            # Compute ra correction
            # a = -3.12515e-6
            # b = -0.00094107
            # c = 0.0328361
            # delta_ra = a * ra_decimal_deg ** 2 + b * ra_decimal_deg + c
            # Compute dec correction
            # a = -5.92991e-5
            # b = 0.000952782
            # c = 0.0588074
            # delta_dec = a * dec_decimal_deg**2 + b * dec_decimal_deg + c
            objects_ra.append(ra_decimal_deg)
            objects_dec.append(dec_decimal_deg)
        # Objects coordinates on J2000 epoch
        self.cataloge_equatorial_coords = SkyCoord(ra=objects_ra, dec=objects_dec,
                                                   frame=FK5(equinox="J2000"), unit="deg")
        # Compute coordinates on current epoch
        frame = FK5(equinox=f"J{get_decimal_year()}")
        self.equatorial_coords = self.cataloge_equatorial_coords.transform_to(frame=frame)

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        self.inds_on_plot = [i for i in np.where(zenith_distance < np.pi/2)[0]
                             if self.objects_visibility[self.objects_names[i]] == 1]
        x = zenith_distance[self.inds_on_plot] * np.sin(azimuth[self.inds_on_plot])
        y = - zenith_distance[self.inds_on_plot] * np.cos(azimuth[self.inds_on_plot])
        self.x_on_plot = x
        self.y_on_plot = y
        plt_inst = ax.plot(x, y, "r*", markersize=3)[0]
        for i, idx in enumerate(self.inds_on_plot):
            x_text = x[i]
            y_text = y[i]
            dists = np.hypot(x-x_text, y-y_text)
            nearest_idx = minimum_position(np.where(dists != 0, dists, 100))
            x_nearest = x[nearest_idx]
            y_nearest = y[nearest_idx]
            if x_text > x_nearest:
                horizontalalignment = "right"
            else:
                horizontalalignment = "left"
            if y_text > y_nearest:
                verticalalignment = "bottom"
            else:
                verticalalignment = "top"
            p = ax.annotate(self.objects_names[idx], xy=(x[i], y[i]), size=8,
                            verticalalignment=verticalalignment,
                            horizontalalignment=horizontalalignment)
            self.plot_instances.append(p)
        self.plot_instances.append(plt_inst)

    def save(self):
        with open(self.visibility_file, "w") as fout:
            json.dump(dict(self.objects_visibility), fout)
