#! /usr/bin/env python

import time
import threading
import numpy as np
from scipy.ndimage import minimum_position
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from astropy import units
from astropy.time import Time
from astropy.coordinates import AltAz
from astropy.coordinates import FK5

from PyQt5.QtWidgets import QSizePolicy
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QMutex

from .CoordinateGrid import CoordinateGrid
from .Constellations import Constellations
from .Stars import Stars
from .ObsObjects import ObsObjects
from .SkyMark import SkyMark
from .Moon import Moon
from .Planets import Planets
from .Mast import Mast
from .suppl_funcs import get_decimal_year


# Use only type 1 fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['ps.useafm'] = False
matplotlib.rcParams['text.usetex'] = False


class SkyPlot(FigureCanvas):
    selected_object_update = pyqtSignal(dict)
    object_selected = pyqtSignal(dict)
    mutex = QMutex()

    def __init__(self, parent=None):
        self.main_window = parent
        self.fig, self.ax = plt.subplots(figsize=(8, 8), dpi=100)
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.plot_instances = []

        # Configure plot
        self.fig.subplots_adjust(left=0.0, bottom=0, right=1.0, top=1, wspace=None, hspace=None)
        self.ax.axis("off")
        self.ax.set_aspect(1.0)
        self.zoom = 1.0
        self.default_range = 1.72
        self.time_shift = 0  # In hours
        self.selected_object_name = None  # The name of the object the user has clicked on

        # Load celestial sphere objects
        self.components_to_plot = []
        self.cgrid = CoordinateGrid(self.main_window.site_location)
        self.components_to_plot.append(self.cgrid)
        self.stars = Stars(self.main_window.site_location)
        self.components_to_plot.append(self.stars)
        self.constellations = Constellations(self.main_window.site_location)
        self.components_to_plot.append(self.constellations)
        self.obs_objects = ObsObjects(self.main_window.site_location)
        self.components_to_plot.append(self.obs_objects)
        self.click_mark = SkyMark(self.main_window.site_location, "click")
        self.components_to_plot.append(self.click_mark)
        self.moon = Moon(self.main_window.site_location)
        self.components_to_plot.append(self.moon)
        self.planets = Planets(self.main_window.site_location)
        self.components_to_plot.append(self.planets)
        self.telescope_mark = SkyMark(self.main_window.site_location, "telescope")
        self.components_to_plot.append(self.telescope_mark)
        self.mast = Mast(self.main_window.site_location)
        self.components_to_plot.append(self.mast)

        # Connect mouse events
        self.fig.canvas.mpl_connect('scroll_event', self.on_scroll)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

        # If self.keep_updated is True, the skyplot will be updated once a minute
        # to maintain an actual view of the rotating celestial sphere
        self.keep_updated = True
        threading.Thread(target=self.auto_updater).start()

    def on_scroll(self, event):
        plot_range = 2 * self.default_range / self.zoom
        x_click = event.xdata
        y_click = event.ydata
        if event.button == "up":
            # Zooming in
            if x_click is None:
                return
            self.zoom *= 1.25

        elif event.button == "down":
            # Zoomint out
            self.zoom = max(1, self.zoom/1.25)
            if x_click is None:
                x_click = 0
                y_click = 0

        x_lim_min, x_lim_max = self.ax.get_xlim()
        y_lim_min, y_lim_max = self.ax.get_ylim()

        x_frac = (x_click - x_lim_min) / plot_range
        y_frac = (y_click - y_lim_min) / plot_range

        plot_range_new = 2*self.default_range / self.zoom

        if self.zoom > 1:
            x_lim_min_new = max(-self.default_range, x_click - x_frac*plot_range_new)
            x_lim_max_new = min(self.default_range, x_click + (1-x_frac)*plot_range_new)
            y_lim_min_new = max(-self.default_range, y_click - y_frac*plot_range_new)
            y_lim_max_new = min(self.default_range,  y_click + (1-y_frac) * plot_range_new)
        else:
            x_lim_min_new = -self.default_range
            x_lim_max_new = self.default_range
            y_lim_min_new = -self.default_range
            y_lim_max_new = self.default_range

        self.ax.set_xlim(x_lim_min_new, x_lim_max_new)
        self.ax.set_ylim(y_lim_min_new, y_lim_max_new)
        # Set new lim_mag
        print(self.zoom)
        if 1.0 <= self.zoom <= 1.5:
            self.stars.set_lim_mag(4)
        elif 1.5 < self.zoom <= 2.5:
            self.stars.set_lim_mag(5)
        elif 2.5 < self.zoom <= 4:
            self.stars.set_lim_mag(6)
        elif 4 < self.zoom <= 6:
            self.stars.set_lim_mag(7)
        else:
            self.stars.set_lim_mag(8)
        self.stars.compute_altaz(self.current_time)
        self.plot()

    def on_click(self, event):
        """
        Function looks what mouse button was clicked and calls an appropriete function
        """
        if event.button in (1, 3):
            # Left or right mouse button clicked
            if event.xdata is None:
                return
            include_stars = False if event.button == 1 else True
            obj_name, obj_ra, obj_dec = self.search_nearest(event.xdata, event.ydata, include_stars)
            self.selected_object_name = obj_name
            self.click_mark.set_coordinates(obj_name, obj_ra, obj_dec)
            self.click_mark.compute_altaz(self.current_time)
            obj_params = self.click_mark.get_params()
            self.object_selected.emit(obj_params)
            self.selected_object_update.emit(obj_params)
            self.plot()

    def search_nearest(self, xdata, ydata, include_stars=False):
        """
        Function finds the nearest object to a given point on the plot. If include_stars is True,
        the stars are included in the search. Otherwise, the search goes only over obs objects
        """
        dists_to_obs_objects = np.hypot(xdata-self.obs_objects.x_on_plot, ydata-self.obs_objects.y_on_plot)
        nearest_obj_idx = minimum_position(np.where(dists_to_obs_objects != 0, dists_to_obs_objects, 100))[0]
        nearest_obj_idx_on_plot = self.obs_objects.inds_on_plot[nearest_obj_idx]
        dist_to_nearest_obs_obj = dists_to_obs_objects[nearest_obj_idx]
        obs_obj_name = self.obs_objects.objects_names[nearest_obj_idx_on_plot]
        coords_of_nearest_object = self.obs_objects.equatorial_coords[nearest_obj_idx_on_plot]
        if include_stars is True:
            dists_to_stars = np.hypot(xdata-self.stars.x_on_plot, ydata-self.stars.y_on_plot)
            nearest_star_idx = minimum_position(np.where(dists_to_stars != 0, dists_to_stars, 100))[0]
            nearest_star_idx_on_plot = self.stars.inds_on_plot[nearest_star_idx]
            dist_to_nearest_star = dists_to_stars[nearest_star_idx]
            if dist_to_nearest_obs_obj < dist_to_nearest_star:
                # The closest object to the click point is an obs object
                return obs_obj_name, coords_of_nearest_object.ra, coords_of_nearest_object.dec
            elif dist_to_nearest_star < (0.025 * self.default_range/self.zoom):
                # The closest object is a star, and it is located not too far from the
                # click point
                star_name = self.stars.objects_names[nearest_star_idx_on_plot]
                coords_of_nearest_star = self.stars.equatorial_coords[nearest_star_idx_on_plot]
                return star_name, coords_of_nearest_star.ra, coords_of_nearest_star.dec
            else:
                # Distance to the nearest object is too big. Returning point on the sky instead
                # Compute horizontal coordinates of the point under cursor by the coordinates
                # on the plot
                point_azimuth = np.arctan2(-xdata, ydata) - np.pi
                if abs(point_azimuth) > 0.1:
                    point_zenith_distance = xdata / np.sin(point_azimuth)
                else:
                    point_zenith_distance = -ydata / np.cos(point_azimuth)
                point_azimuth = np.degrees(point_azimuth + np.pi)
                point_altitude = 90 - np.degrees(point_zenith_distance)
                point_horizonthal_coordinates = AltAz(alt=point_altitude*units.degree, az=point_azimuth*units.degree,
                                                      obstime=self.current_time,
                                                      location=self.main_window.site_location)
                # Compute equatorial coordinates by the horizontal
                frame_equatorial = FK5(equinox="J%1.2f" % get_decimal_year())
                point_equatorial_coordinates = point_horizonthal_coordinates.transform_to(frame_equatorial)
                return "Point under cursor", point_equatorial_coordinates.ra, point_equatorial_coordinates.dec
        else:
            return obs_obj_name, coords_of_nearest_object.ra, coords_of_nearest_object.dec

    def update_coordinates(self):
        """
        Set time for which the plot to make. If no time is given as an
        argument, the current time is used
        """
        self.current_time = Time.now() + self.time_shift * units.hour
        for component in self.components_to_plot:
            component.compute_altaz(self.current_time)

    def plot(self):
        """
        Plot all components
        """
        self.mutex.lock()
        while self.plot_instances:
            self.plot_instances.pop().remove()
        for component in self.components_to_plot:
            component.plot(self.ax)
        # Show time shift value on the plot for nonzero shift
        if self.time_shift != 0:
            ann = self.ax.annotate("dT=%+i" % self.time_shift, xy=(0.01, 0.98), xycoords="axes fraction",
                                   size=16, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round'),
                                   verticalalignment="top", horizontalalignment="left")
            self.plot_instances.append(ann)
        self.draw()
        self.mutex.unlock()

    @pyqtSlot(int)
    def shift_time(self, shift):
        """
        Shifts time by shift hours. If shift is, sets the
        shift to be zero.
        """
        if shift == 0:
            self.time_shift = 0
        else:
            self.time_shift += shift
        self.update_coordinates()
        if self.selected_object_name is not None:
            # The user has clicked on some object so we want to  update the object
            # parameters (horizontal coordinates and the airmass) once a minute
            obj_params = self.click_mark.get_params()
            self.selected_object_update.emit(obj_params)
        self.plot()

    def auto_updater(self):
        while 1:
            if self.keep_updated is True:
                self.update_coordinates()
                self.plot()

            if self.selected_object_name is not None:
                # The user has clicked on some object so we want to  update the object
                # parameters (horizontal coordinates and the airmass) once a minute
                obj_params = self.click_mark.get_params()
                self.selected_object_update.emit(obj_params)
            for _ in range(5):
                if not self.main_window.is_alive:
                    return
                time.sleep(1)

    def move_telescope(self, coords):
        ra_h = int(coords['ra_h'])
        ra_m = abs(int(coords['ra_m']))
        ra_s = abs(int(coords['ra_s']))
        ra_decimal_deg = 15 * ra_h + 15*ra_m/60 + 15*ra_s/3600

        dec_d = int(coords['dec_d'])
        dec_m = abs(int(coords['dec_m']))
        dec_s = abs(int(coords['dec_s']))
        if dec_d >= 0:
            dec_decimal_deg = dec_d + dec_m / 60 + dec_s / 3600
        else:
            dec_decimal_deg = dec_d - dec_m / 60 - dec_s / 3600

        if (ra_decimal_deg != self.telescope_mark.current_ra) or (dec_decimal_deg != self.telescope_mark.current_dec):
            self.telescope_mark.set_coordinates("telescope", ra_decimal_deg, dec_decimal_deg)
            self.telescope_mark.compute_altaz(self.current_time)
            self.plot()
