import threading
import time
from astropy.time import Time
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal
# from astropy.utils import iers
# iers.conf.auto_download = False


class SiderealClock(QtWidgets.QWidget):
    time_update = pyqtSignal(dict)

    def __init__(self, main_window):
        super().__init__(main_window)
        self.main_window = main_window
        threading.Thread(target=self.clock).start()

    def clock(self):
        while self.main_window.is_alive:
            time.sleep(1.0)
            t = Time(Time.now(), location=self.main_window.site_location)
            t.delta_ut1_utc = 0
            sidereal_time = t.sidereal_time("mean",).hms
            hms = {"h": sidereal_time.h,
                   "m": sidereal_time.m,
                   "s": sidereal_time.s}
            self.time_update.emit(hms)
