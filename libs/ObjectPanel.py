#! /usr/bin/env python

from astropy.coordinates import Angle
from astropy import units
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QSizePolicy


class ObjectPanel(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        self.main_layout = QtWidgets.QVBoxLayout()
        self.ra_hours = None
        panel_label = QtWidgets.QLabel("Selected object")
        panel_label.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Bold))
        panel_label.setStyleSheet("color: green")
        self.main_layout.addWidget(panel_label)
        default_font = QtGui.QFont("Arial", 10)
        default_font_metric = QtGui.QFontMetrics(default_font)

        # object name layout
        name_layout = QtWidgets.QHBoxLayout()
        name_layout.addWidget(QtWidgets.QLabel("Name:"))
        self.obj_name_label = QtWidgets.QLabel("\u2014")
        name_layout.addWidget(self.obj_name_label, Qt.AlignLeft)

        # Calculate the largest label width (it is the azimuth label) to set all labels
        # widths to be equal to this width
        width = default_font_metric.width("180\u00B0 00' 00''") + 5
        # Equatorial coordinates layout
        equatorial_coords_layout = QtWidgets.QHBoxLayout()
        # Alpha
        alpha_label = QtWidgets.QLabel("\u03B1:")
        alpha_label.setFixedWidth(default_font_metric.width("\u03B1:"))
        equatorial_coords_layout.addWidget(alpha_label, Qt.AlignLeft)
        self.alpha_value_label = QtWidgets.QLabel("\u2014\u02b0 \u2014\u1d50 \u2014\u02e2")
        self.alpha_value_label.setFont(default_font)
        self.alpha_value_label.setFixedWidth(width)
        equatorial_coords_layout.addWidget(self.alpha_value_label, Qt.AlignLeft)
        # Delta
        delta_label = QtWidgets.QLabel("\u03B4:")
        delta_label.setFixedWidth(default_font_metric.width("\u03B4:"))
        equatorial_coords_layout.addWidget(delta_label, Qt.AlignLeft)
        self.delta_value_label = QtWidgets.QLabel("\u2014\u00B0 \u2014' \u2014''")
        self.delta_value_label.setFont(default_font)
        self.delta_value_label.setFixedWidth(width)
        equatorial_coords_layout.addWidget(self.delta_value_label, Qt.AlignCenter)
        equatorial_coords_layout.addStretch()

        # Horizontal coordinates layout
        horizonthal_coords_layout = QtWidgets.QHBoxLayout()
        # Azimuth
        azimuth_label = QtWidgets.QLabel("A:")
        azimuth_label.setFixedWidth(default_font_metric.width("A:"))
        horizonthal_coords_layout.addWidget(azimuth_label, Qt.AlignLeft)
        self.azimuth_value_label = QtWidgets.QLabel("\u2014\u00B0 \u2014' \u2014''")
        self.azimuth_value_label.setFont(default_font)
        self.azimuth_value_label.setFixedWidth(width)
        horizonthal_coords_layout.addWidget(self.azimuth_value_label, Qt.AlignLeft)
        # Altitude
        altitude_label = QtWidgets.QLabel("h:")
        altitude_label.setFixedWidth(default_font_metric.width("h:"))
        horizonthal_coords_layout.addWidget(altitude_label, Qt.AlignLeft)
        self.altitude_value_label = QtWidgets.QLabel("\u2014\u00B0 \u2014' \u2014''")
        self.altitude_value_label.setFont(default_font)
        self.altitude_value_label.setFixedWidth(width)
        horizonthal_coords_layout.addWidget(self.altitude_value_label, Qt.AlignLeft)
        horizonthal_coords_layout.addStretch()

        # Sidereal time and hour angle layout
        times_layout = QtWidgets.QHBoxLayout()
        # Sidereal time
        sidereal_time_label = QtWidgets.QLabel("S:")
        sidereal_time_label.setFixedWidth(default_font_metric.width("S:"))
        times_layout.addWidget(sidereal_time_label, Qt.AlignLeft)
        self.sidereal_time_value_label = QtWidgets.QLabel("\u2014\u00B0 \u2014' \u2014''")
        self.sidereal_time_value_label.setFont(default_font)
        self.sidereal_time_value_label.setFixedWidth(width)
        times_layout.addWidget(self.sidereal_time_value_label, Qt.AlignLeft)
        # Hour angle
        hour_angle_label = QtWidgets.QLabel("t:")
        hour_angle_label.setFixedWidth(default_font_metric.width("t:"))
        times_layout.addWidget(hour_angle_label, Qt.AlignLeft)
        self.hour_angle_value_label = QtWidgets.QLabel("\u2014\u02b0 \u2014\u1d50 \u2014\u02e2")
        self.hour_angle_value_label.setFont(default_font)
        self.hour_angle_value_label.setFixedWidth(width)
        times_layout.addWidget(self.hour_angle_value_label, Qt.AlignLeft)
        times_layout.addStretch()

        # Airmass layout
        airmass_layout = QtWidgets.QHBoxLayout()
        airmass_layout.addWidget(QtWidgets.QLabel("z:"))
        self.airmass_value_label = QtWidgets.QLabel("\u2014")
        airmass_layout.addWidget(self.airmass_value_label, Qt.AlignLeft)

        self.main_layout.addLayout(name_layout)
        self.main_layout.addLayout(equatorial_coords_layout)
        self.main_layout.addLayout(horizonthal_coords_layout)
        self.main_layout.addLayout(airmass_layout)
        self.main_layout.addLayout(times_layout)
        self.setLayout(self.main_layout)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def set_object_parameters(self, obj_params):
        self.obj_name_label.setText(obj_params["name"])
        self.ra_hours = obj_params['ra_h'] + obj_params['ra_m'] / 60 + obj_params['ra_s'] / 3600
        self.alpha_value_label.setText("%i\u02b0 %i\u1d50 %i\u02e2" % (obj_params['ra_h'], obj_params['ra_m'],
                                                                       obj_params['ra_s']))
        self.delta_value_label.setText("%i\u00B0 %i' %i''" % (obj_params['dec_d'], abs(obj_params['dec_m']),
                                                              abs(obj_params['dec_s'])))
        self.azimuth_value_label.setText("%i\u00B0 %i' %i''" % (obj_params['az_d'], obj_params['az_m'],
                                                                obj_params['az_s']))
        self.altitude_value_label.setText("%i\u00B0 %i' %i''" % (obj_params['alt_d'], obj_params['alt_m'],
                                                                 obj_params['alt_s']))
        self.airmass_value_label.setText("%1.3f" % obj_params["airmass"])

    def set_sidereal_time(self, hms):
        self.sidereal_time_value_label.setText("%i\u02b0 %i\u1d50 %i\u02e2" % (hms['h'], hms['m'], hms['s']))
        if self.ra_hours is not None:
            s_time_hours = hms['h'] + hms['m']/60 + hms['s']/3600
            hour_angle = s_time_hours - self.ra_hours
            if hour_angle < 0:
                hour_angle += 24
            hour_angle = Angle(hour_angle, units.hour)
            h, m, s = hour_angle.hms
            self.hour_angle_value_label.setText("%i\u02b0 %i\u1d50 %i\u02e2" % (h, m, s))
