#! /usr/bin/env python

import time
import numpy as np
from scipy.ndimage import minimum_position
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from astropy.coordinates import AltAz
from astropy.coordinates import get_body
from .SkyObject import SkyObject
from .suppl_funcs import get_decimal_year


class Planets(SkyObject):
    """
    A class for plotting objects
    """
    def __init__(self, location):
        super().__init__(location)
        self.objects_names = ['mercury', 'venus', 'earth-moon-barycenter', 'earth',
                              'mars', 'jupiter', 'saturn', 'uranus', 'neptune']
        ra = [0.0 for _ in self.objects_names]
        dec = [-90 for _ in self.objects_names]
        self.equatorial_coords = SkyCoord(ra=ra, dec=dec, frame=FK5(equinox="J2000"), unit="deg")
        self.last_update = 0.0

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        self.inds_on_plot = np.where(zenith_distance < np.pi/2)[0]
        x = zenith_distance[self.inds_on_plot] * np.sin(azimuth[self.inds_on_plot])
        y = - zenith_distance[self.inds_on_plot] * np.cos(azimuth[self.inds_on_plot])
        self.x_on_plot = x
        self.y_on_plot = y
        plt_inst = ax.plot(x, y, "b*", markersize=4)[0]
        for i, idx in enumerate(self.inds_on_plot):
            x_text = x[i]
            y_text = y[i]
            dists = np.hypot(x-x_text, y-y_text)
            nearest_idx = minimum_position(np.where(dists != 0, dists, 100))
            x_nearest = x[nearest_idx]
            y_nearest = y[nearest_idx]
            if x_text > x_nearest:
                horizontalalignment = "right"
            else:
                horizontalalignment = "left"
            if y_text > y_nearest:
                verticalalignment = "bottom"
            else:
                verticalalignment = "top"
            p = ax.annotate(self.objects_names[idx], xy=(x[i], y[i]), size=8,
                            verticalalignment=verticalalignment,
                            horizontalalignment=horizontalalignment)
            self.plot_instances.append(p)
        self.plot_instances.append(plt_inst)

    def update_equatorial(self, obstime):
        ra = []
        dec = []
        for name in self.objects_names:
            body = get_body(name, time=obstime, location=self.location)
            ra.append(body.ra)
            dec.append(body.dec)
        self.equatorial_coords = SkyCoord(ra=ra, dec=dec, frame=FK5(equinox="J%1.2f" % get_decimal_year()), unit="deg")
        self.last_update = time.time()

    def compute_altaz(self, obstime):
        if time.time() - self.last_update > 3*3600:
            # We only want to update the planets coordinates once in three hours
            self.update_equatorial(obstime)
        frame = AltAz(obstime=obstime, location=self.location)
        self.horizonthal_coords = self.equatorial_coords.transform_to(frame)
