#! /usr/bin/env python

from astropy.coordinates import AltAz
# from astropy.utils import iers
# iers.conf.auto_download = False


class SkyObject(object):
    """
    A base class for all the object that can be plotted on the celestial sphere.
    In contains equatorial coordinates of an object, a method to compute current
    horizontal coordinates, etc.
    """
    def __init__(self, location):
        self.equatorial_coords = None
        self.plot_instances = []
        self.location = location

    def compute_altaz(self, obstime):
        """
        Computes form equatorial to current alt-az coordinates
        """
        frame = AltAz(obstime=obstime, location=self.location)
        self.horizonthal_coords = self.equatorial_coords.transform_to(frame)

    def plot(self):
        """
        Plotting function. Should be implemented for each concrete class.
        """
        raise NotImplementedError

    def remove(self):
        """
        Removes the object from the celestial sphere plot
        """
        while self.plot_instances:
            self.plot_instances.pop().remove()
